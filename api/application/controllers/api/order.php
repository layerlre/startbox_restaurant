<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require (APPPATH.'/libraries/Rest/http/Request.php');
require (APPPATH.'/libraries/Rest/http/Response.php');
require (APPPATH.'/libraries/Rest/rest/Controller.php');


class Order extends yidas\rest\Controller {

	public function __construct() {
		parent::__construct();

		$this->load->library("EscPos.php");
	}


	public function keepOrder(){
		$data = $this->request->input();

		$outOfStock = array();

		if(!$this->existHold($data["table_id"])){
			return $this->response->json("INVALID_HOLD", 400);
		}
		
		foreach($data["details"] as $detail){

			$this->db->select('*');
			$this->db->from('posales');
			$this->db->where(
				array(
						'product_id'=> $detail["product_id"]
						, 'table_id' => $data["table_id"]
						, 'cookstatus' => 1
					)
			);
			$old_sale = $this->db->get()->result();
	
			$this->db->select('*');
			$this->db->from('products');
			$this->db->where('id', $detail["product_id"]);
			$product = $this->db->get()->result()[0];

			$query = "select quantity from stocks where product_id = ". $product->id ." and store_id = ". $data["store_id"];
			$qty = $this->db->query($query)->result()[0]->quantity;

			$query = "  select sum(ps.qt) qt ";
			$query .= " from posales ps ";
			$query .= "			inner join registers r on ps.register_id = r.id";
			$query .= "			inner join stocks s on s.store_id = ". $data["store_id"] ." and s.product_id = ps.product_id ";
			$query .= "	where ps.product_id = ". $product->id ;
			$res = $this->db->query($query)->result();
			if($res){
				$qty -= $res[0]->qt;
			}
			if(($qty - $detail["order_qty"] >= 0 && $product->type == 0) || $product->type == 1){

				if(count($old_sale) > 0){
	
					$old_sale = $old_sale[0];
		
					$this->db->where('id', $old_sale->id);
					$this->db->update(
						'posales'
						, array(
								'qt' => ($old_sale->qt + $detail["order_qty"])
								, 'price' => $product->price
							)
					);
					
					$this->db->where('posales_id', $old_sale->id);
					$this->db->update(
						'posalecook'
						, array(
								'qt' => ($old_sale->qt + $detail["order_qty"])
						  )
					);
		
				} else{
					$posale = array(
						'product_id' => $product->id
						, 'name' => $product->name
						, 'price' => $product->price
						, 'table_id'=> $data["table_id"]
						, 'optbuffet' => 0
						, 'user_id'=> $data["user_id"]
						, 'register_id'=> $data["register_id"]
						, 'qt'=> $detail["order_qty"]
						, 'cookstatus'=> 1
						, 'status'=> 0
						, 'number' => 1
						, 'created_at' => date('Y-m-d H:i:s')
					);
		
					$this->db->insert('posales',$posale);
		
					$posale_id = $this->db->insert_id();
		
					$posalecook = array(
						'posales_id' => $posale_id
						, 'product_id' => $product->id
						, 'name' => $product->name
						, 'price' => $product->price
						, 'qt'=> $detail["order_qty"]
						, 'register_id'=> $data["register_id"]
						, 'number' => 1
						, 'table_id'=> $data["table_id"]
						, 'status' => 0
						, 'timedate' => date('Y-m-d H:i:s')
						, 'user_id'=> $data["user_id"]
					);
					$this->db->insert('posalecook',$posalecook);
				}

			}
			else{

				array_push($outOfStock,$product);
			}

		}

		// $query = " update stocks set quantity = quantity-1 where product_id = ".$product->id ." and store_id = ". $data["store_id"];
		// $this->db->query($query);

		return $this->response->json($outOfStock, 200);
	}

	public function getCurrentOrder($table_id){

		if(!$this->existHold($table_id)){
			return $this->response->json("INVALID_HOLD", 400);
		}

		$query = " SELECT pos.id, p.id product_id, p.name, pos.options, pos.qt,p.photothumb,pos.cookstatus,pos.table_id"
			   . " FROM posales pos inner join products p on pos.product_id = p.id"
			   . " WHERE pos.table_id = ".$table_id
			   . " order by pos.created_at desc ";
		$orders = $this->db->query($query)->result();

		foreach ($orders as $order)
		{
			$query = " SELECT op.id, op.optionname, op.optionprice, IFNULL(pos_op.optqt,0) qty "
				   . " FROM productoptions op left join posale_option pos_op on op.id = pos_op.option_id"
				   . " WHERE product_id = ". $order-> product_id
				   . " ORDER BY op.id ";
			$order->product_options = $this->db->query($query)->result();
		}

		return $this->response->json($orders, 200);

	}

	public function addOption(){

		$data = $this->request->input();

		if(!$this->existHold($data["table_id"])){
			return $this->response->json("INVALID_HOLD", 400);
		}

		$this->db->where('posale_id', $data["posale_id"]);
		$this->db->delete('posale_option');

		foreach ( $data["options"] as $option )
		{
			$this->db->select('*');
			$this->db->from('productoptions');
			$this->db->where('id',$option["option_id"]);
			$op = $this->db->get()->result()[0];

			$ins = array(
				'posale_id' => $data["posale_id"]
				, 'option_id' => $option["option_id"]
				, 'optname' => $op->optionname
				, 'optqt' =>  $option["qty"]
				, 'optprice'=> $op->optionprice
				, 'store_id'=> $data["store_id"]
				, 'table_id'=> $data["table_id"]
				, 'register_id'=> $data["register_id"]
				, 'created_at' => date('Y-m-d H:i:s')
			);

    		$this->db->insert('posale_option',$ins);

		}

		$this->db->where('id', $data["posale_id"]);
		$this->db->update('posales',array('options'=>$data["option_text"]));

		$this->db->where('posales_id', $data["posale_id"]);
		$this->db->update('posalecook',array('options'=>$data["option_text"]));
	
		return $this->response->json("OK", 200);

	}

	public function deleteItem($posale_id){

		$this->db->select('*');
		$this->db->from('posalecook');
		$this->db->where('posales_id', $posale_id);
		$posalecook = $this->db->get()->result()[0];

		$this->db->select('*');
		$this->db->from('tablecooks');
		$this->db->where('posales_id',  $posale_id);
		$tblCook = $this->db->get()->result();

		if(!$this->existHold($posalecook->table_id)){
			return $this->response->json("INVALID_HOLD", 400);
		}

		$this->db->where('posale_id', $posale_id);
		$this->db->delete('posale_option');

		$this->db->where('posales_id', $posale_id);
		$this->db->delete('posalecook');

		$this->db->where('id', $posale_id);
		$this->db->delete('posales');

		$this->db->where('posales_id', $posale_id);
		$this->db->delete('tablecooks');

		$this->db->select('*');
		$this->db->from('tables');
		$this->db->where('id', $posalecook->table_id);
		$table = $this->db->get()->result()[0];

		// $query 	= " update stocks set quantity = quantity+".$posalecook->qt."  where product_id = ".$posalecook->product_id . " and store_id = ". $table->store_id;
		// $this->db->query($query);
		
		if(count($tblCook) > 0){
			$tblCook = $tblCook[0];

			$log = "Cancel order ".$tblCook->name." (".$tblCook->qt.')';
			if($tblCook->options){
				$log.= " with option ".$tblCook->options;
			}
			
			$this->keepLog($log);
		}

		return $this->response->json("OK", 200);
	}

	public function submitOrder(){

		$data = $this->request->input();

		$this->db->select('*');
		$this->db->from('posalecook');
		$this->db->where('posales_id', $data["id"]);

		$posalecook = $this->db->get()->result()[0];

		if(!$this->existHold($posalecook->table_id)){
			return $this->response->json("INVALID_HOLD", 400);
		}

		$this->submit($posalecook,$data['user_id']);
		$this->print($posalecook);

		return $this->response->json("OK", 200);

	}

	public function submitAllOrder(){

		$data = $this->request->input();

		if(!$this->existHold($data["table_id"])){
			return $this->response->json("INVALID_HOLD", 400);
		}

		$this->db->select('*');
		$this->db->from('posalecook');
		$this->db->where(
			array(
				'table_id'=> $data["table_id"]
				, 'status'=> 0
			)
		);

		$posalecooks = $this->db->get()->result();

		foreach( $posalecooks as $posalecook ){
			$this->submit($posalecook,$data['user_id']);
			$this->print($posalecook);
		}

		return $this->response->json("OK", 200);

	}

	public function submit($posalecook,$user_id){
		$tablecooks = array(
			'posales_id' => $posalecook->posales_id
			, 'product_id' => $posalecook->id
			, 'name' => $posalecook->name
			, 'price' => $posalecook->price
			, 'qt'=> $posalecook->qt
			, 'register_id'=> $posalecook->register_id
			, 'number' => $posalecook->number
			, 'table_id'=> $posalecook->table_id
			, 'status' => 1
			, 'timedate' => date('Y-m-d H:i:s')
			, 'options' => $posalecook->options
			, 'user_id'=> $user_id
		);
		
		$this->db->insert('tablecooks',$tablecooks);

		$this->db->where('id', $posalecook->posales_id);
		$this->db->update(
			'posales'
			, array('
				cookstatus'=> 2 
				, 'status' => 1
			)
		);

		$this->db->where('posales_id', $posalecook->posales_id);
		$this->db->update('posalecook', array('status' => 1));

		$log = "Add Order ".$posalecook->name." (". $posalecook->qt.")";
		if($posalecook->options){
			$log.= " with option ".$posalecook->options;
		}
		$this->keepLog($log);

		//return $this->response->json("OK", 200);
	}

	public function existHold($table_id){
		$this->db->select('*');
		$this->db->from('holds');
		$this->db->where('table_id', $table_id);
		return count($this->db->get()->result()) > 0;
	}

	public function keepLog($message){

		$user_id = $this->input->request_headers()["UserID"];

		if($user_id){

			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('id', $user_id);
			$user = $this->db->get()->result()[0];

			$ins = array(
				'created_at' => date('Y-m-d H:i:s')
				, 'user_name' => $user->username
				, 'store_id' => $user->store_id
				, 'logmessage'=> $message.' '.date('Y-m-d H:i:s').' by '.$user->username
				, 'status' => 1
			);

			$this->db->insert('loghistory',$ins);
		}
	}


	public function print($posalecook){

		try {

			$this->db->select('*');
			$this->db->from('products');
			$this->db->where('id', $posalecook->product_id);
			$product = $this->db->get()->result()[0];

			$this->db->select('*');
			$this->db->from('categories');
			$this->db->where('name', $product->category);
			$category = $this->db->get()->result()[0];

			$this->db->select('*');
			$this->db->from('tables');
			$this->db->where('id', $posalecook->table_id);
			$table = $this->db->get()->result()[0];

			$this->db->select('*');
			$this->db->from('registers');
			$this->db->where('id', $posalecook->register_id);
			$register = $this->db->get()->result()[0];

			$this->db->select('*');
			$this->db->from('stores');
			$this->db->where('id', $register->store_id);
			$store = $this->db->get()->result()[0];

			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('id', $this->input->request_headers()["UserID"]);
			$user = $this->db->get()->result()[0];

			if($category->printer){

				$r1 ="โต๊ะบริการ : ".trim($table->name);
				$r2 = "บริการ : " . $user->firstname . " " . $user->lastname . " " . date('d-m-Y H:i:s');
				$r3 = $posalecook->name . " (" . $posalecook->qt . ")";
				$r4 = "No.: " . $product->code;
				$r5 = $posalecook->options;

				$connector = new Escpos\PrintConnectors\WindowsPrintConnector($category->printer);
				$profile = Escpos\CapabilityProfiles\DefaultCapabilityProfile::getInstance();
				$buffer = new Escpos\PrintBuffers\ImagePrintBuffer();
	
				$printer = new Escpos\Printer($connector,$profile);
				$printer->setPrintBuffer($buffer);
				$printer->setFontSize(38);
				$printer->text($this->bulidRow($r1,true,38));
				$printer->setFontSize(24);
				$printer->text($this->bulidRow($r2,true,55));
				$printer->setFontSize(40);
				$printer->text($this->bulidRow($r3,true,37));
				$printer->setFontSize(40);
				$printer->text($this->bulidRow($r4,true,37));
				$printer->feed();
				$printer->setFontSize(24);
				$printer->text($this->bulidRow($r5,false,55));
				$printer->feed();
				$printer->feed();
				$printer->cut();
				$printer->close();

			}
		
		} catch (Exception $e) {
			echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
		}

		//sleep(2);

	}

	
	public function bulidRow($text,$center,$maxChar){
		

		$char = array('่','้','๊','๋','ั','์','็','ิ','ี','ึ','ื','ุ','ู');

		$txtArr = preg_split('//u',$text, null, PREG_SPLIT_NO_EMPTY);

		$charCount = 0;
		$str="";

		for($i = 0; $i < count($txtArr); $i++) {
			if($charCount < $maxChar){
				$str .= $txtArr[$i];
				if(!array_search($txtArr[$i], $char)){
					$charCount +=1;
				}
			}
		}

		if($center){

			$centerIndex = ($maxChar/2)-($charCount/2);
			$empty = "";
			for($i = 0; $i < $centerIndex; $i++) {
				$empty .= " "; 
			}
			$str = $empty. $str;
			return $str;

		}else{

		}


		return $str;
	
	}

}
