<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require (APPPATH.'/libraries/Rest/http/Request.php');
require (APPPATH.'/libraries/Rest/http/Response.php');
require (APPPATH.'/libraries/Rest/rest/Controller.php');

class Ping extends yidas\rest\Controller {

	public function __construct() {
		parent::__construct();
		
	}
	
	public function index()
	{
		return $this->response->json("OK", 200);
	}


}
