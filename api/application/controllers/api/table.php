<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require (APPPATH.'/libraries/Rest/http/Request.php');
require (APPPATH.'/libraries/Rest/http/Response.php');
require (APPPATH.'/libraries/Rest/rest/Controller.php');

class Table extends yidas\rest\Controller {

	public function __construct() {
		parent::__construct();
	}

	public function zone($store_id)
	{
		$query = " select * from zones z where exists (select null from tables where zone_id = z.id and z.store_id = ".$store_id." ) ";
		$data = $this->db->query($query)->result();

		return $this->response->json($data, 200);
	}
	
	public function table($zone_id)
	{
		$this->db->select('*');
		$this->db->from('tables');
		$this->db->where('zone_id', $zone_id);
		$query = $this->db->get();

		$data = $query->result();

		return $this->response->json($data, 200);
	}

	public function reserve()
	{
		$data = $this->request->input();

		$this->db->select('*');
		$this->db->from('tables');
		$this->db->where('id', $data["id"]);
		$query = $this->db->get();
		$table = $query->result()[0];

		$update_data = array(
			'status' => 1
			, 'time' => date("h:i")
		);

		$this->db->where('id',$table->id);
		$this->db->update('tables',$update_data);

		$this->db->select('*');
		$this->db->from('registers');
		$this->db->where(array('store_id'=> $table->store_id, 'status' => 1));
		$query = $this->db->get();
		$register = $query->result()[0];
		
		$data = array(
			'time' => $update_data['time']
			, 'table_id'=> $table->id
			, 'register_id'=> $register->id
			, 'number'=> 1
		);

    	$this->db->insert('holds',$data);

		$this->keepLog("Open table ".$table->name);

		
		return $this->response->json("success", 200);
	}

	public function close()
	{
		$data = $this->request->input();

		$update_data = array(
			'status' => 0
			,'time' => null
		);
		
		$this->db->where('id',$data["id"]);
		$this->db->update('tables',$update_data);

		$this->db->where('table_id', $data["id"]);
		$this->db->delete('posale_option');

		$this->db->where('table_id', $data["id"]);
		$this->db->delete('posalecook');

		$this->db->where('table_id', $data["id"]);
		$this->db->delete('posales');

		$this->db->where('table_id', $data["id"]);
		$this->db->delete('holds');

		$this->db->where('table_id', $data["id"]);
		$this->db->delete('tablecooks');

		$this->db->select('*');
		$this->db->from('tables');
		$this->db->where('id', $data["id"]);
		$tbl = $this->db->get()->result()[0];

		$this->keepLog("Close table ".$tbl->name);

		
		return $this->response->json("success", 200);
	}

	public function swap()
	{
		$data = $this->request->input();


		if(!$this->existHold($data["old_table_id"])){
			return $this->response->json("INVALID_HOLD", 400);
		}

		$update_data = array(
			'status' => 0
		);
		$this->db->where('id',$data["old_table_id"]);
		$this->db->update('tables',$update_data);

		$update_data = array(
			'status' => 1
		);
		
		$this->db->where('id',$data["new_table_id"]);
		$this->db->update('tables',$update_data);

		$update_data = array(
			'table_id' => $data["new_table_id"]
		);

		$this->db->where('table_id', $data["old_table_id"]);
		$this->db->update('posale_option',$update_data);

		$this->db->where('table_id', $data["old_table_id"]);
		$this->db->update('posalecook',$update_data);

		$this->db->where('table_id', $data["old_table_id"]);
		$this->db->update('posales',$update_data);
		
		$this->db->where('table_id', $data["old_table_id"]);
		$this->db->update('holds',$update_data);

		$this->db->select('*');
		$this->db->from('tables');
		$this->db->where('id', $data["old_table_id"]);
		$old_tbl = $this->db->get()->result()[0];

		$this->db->select('*');
		$this->db->from('tables');
		$this->db->where('id', $data["new_table_id"]);
		$new_tbl = $this->db->get()->result()[0];

		$this->keepLog("Change table from ".$old_tbl->name." to ".$new_tbl->name);

		
		return $this->response->json("OK", 200);
	}


	public function existHold($table_id){
		$this->db->select('*');
		$this->db->from('holds');
		$this->db->where('table_id', $table_id);
		return count($this->db->get()->result()) > 0;
	}

	public function keepLog($message){

		$user_id = $this->input->request_headers()["UserID"];

		if($user_id){

			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('id', $user_id);
			$user = $this->db->get()->result()[0];

			$ins = array(
				'created_at' => date('Y-m-d H:i:s')
				, 'user_name' => $user->username
				, 'store_id' => $user->store_id
				, 'logmessage'=> $message.' '.date('Y-m-d H:i:s').' by '.$user->username
				, 'status' => 1
			);

			$this->db->insert('loghistory',$ins);
		}
	}
}
