<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require (APPPATH.'/libraries/Rest/http/Request.php');
require (APPPATH.'/libraries/Rest/http/Response.php');
require (APPPATH.'/libraries/Rest/rest/Controller.php');


class Auth extends yidas\rest\Controller {

	public function __construct() {
		parent::__construct();
		
	}

	public function login()
	{
		$user_login = $this->request->input();
		
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('username', $user_login["username"]);
		$query = $this->db->get();

		$data = $query->result();

		if(count($data) == 0){

			return $this->response->text("NF", 400);

		}else{

			$user = $data[0];

			$validate_result = $this->validate_password($user,$user_login["password"]);
			if(!$validate_result){
				return $this->response->text("NF", 400);
			}

			if($user->role == 'ceo' || $user->role == 'user'){
				return $this->response->text("NA", 400);
			}
			
			$this->db->select('*');
			$this->db->from('registers');
			$this->db->where(array(
				'store_id' => $user->store_id,
				'status' => 1
			));
			$regis = $this->db->get()->result();

			if(count($regis) == 0){
				return $this->response->text("SC", 400);
			}
			$regis = $regis[0];


			$this->db->select('*');
			$this->db->from('stores');
			$this->db->where('id', $user->store_id);
			$store = $this->db->get()->result()[0];

			$user_result = new stdClass();
			$user_result->id = $user->id;
			$user_result->firstname = $user->firstname;
			$user_result->lastname = $user->lastname;
			$user_result->avatar = $user->avatar;
			$user_result->store_id = $store->id;
			$user_result->register_id = $regis->id;
			$user_result->store_name = $store->name;

			$this->keepLog($user);

			return $this->response->json($user_result, 200);

		}
	}

	
	//=| Medthod |==============================================================
	private function hash_password($password)
	{
		$salt = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
		$hash = hash('sha256', $salt . $password);

		return $salt . $hash;
	}

	private function validate_password($user, $password)
	{
		if($user->hashed_password == $password){
			return true;
		}
		$salt = substr($user->hashed_password, 0, 64);
		$hash = substr($user->hashed_password, 64, 64);

		$password_hash = hash('sha256', $salt . $password);

		return $password_hash == $hash;
	}
	//===========================================================================

	public function keepLog($user){

		$ins = array(
			'created_at' => date('Y-m-d H:i:s')
			, 'user_name' => $user->username
			, 'store_id' => $user->store_id
			, 'logmessage'=> 'login '. date('Y-m-d H:i:s').' by '.$user->username
			, 'status' => 1
		);

		$this->db->insert('loghistory',$ins);
	

	}
}
