<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require (APPPATH.'/libraries/Rest/http/Request.php');
require (APPPATH.'/libraries/Rest/http/Response.php');
require (APPPATH.'/libraries/Rest/rest/Controller.php');

class Product extends yidas\rest\Controller {

	public function __construct() {
		parent::__construct();
		
	}


	public function category()
	{
		$this->db->select('*');
		$this->db->from('categories');
		$query = $this->db->get();

		$data = $query->result();

		return $this->response->json($data, 200);
	}

	public function product($storeID)
	{
		$data = [];

		$this->db->select('*');
		$this->db->from('categories');
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			$category = $row;

			$query = "  select p.*, (case when  p.type = 0 then (s.quantity - IFNULL((select SUM(ps.qt) qt ";
			$query .= "																		 	 from posales ps ";
			$query .= "																						inner join registers r on ps.register_id = r.id ";
			$query .= "																						inner join stocks ss on ss.store_id = ".$storeID." and ss.product_id = ps.product_id ";
			$query .= "																  		 	 where ps.product_id = p.id),0)) ";
			$query .= "				 else 0 end  ) qty ";
			$query .= "			from products p ";
			$query .= "				inner join stocks s on p.id = s.product_id and s.store_id = ".$storeID." ";
			$query .= "			where  p.category = '".$category->name."'";
		
			$products = $this->db->query($query)->result();
			$category->products = $products;
			array_push($data,$category);
		}



		return $this->response->json($data, 200);
	}

}
