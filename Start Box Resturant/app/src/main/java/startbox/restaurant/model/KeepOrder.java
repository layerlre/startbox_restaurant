package startbox.restaurant.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class KeepOrder {

    @SerializedName("table_id")
    private int table_id;

    @SerializedName("register_id")
    private int register_id;

    @SerializedName("details")
    private List<KeepOrderDetail> details;

    @SerializedName("user_id")
    private int user_id;

    @SerializedName("store_id")
    private int store_id;

    public int getTable_id() {
        return table_id;
    }

    public void setTable_id(int table_id) {
        this.table_id = table_id;
    }

    public int getRegister_id() {
        return register_id;
    }

    public void setRegister_id(int register_id) {
        this.register_id = register_id;
    }

    public List<KeepOrderDetail> getDetails() {
        return details;
    }

    public void setDetails(List<KeepOrderDetail> details) {
        this.details = details;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }
}
