package startbox.restaurant.model;

import com.google.gson.annotations.SerializedName;

public class SubmitOrderPerItem {

    @SerializedName("id")
    private int id;

    @SerializedName("user_id")
    private int user_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
