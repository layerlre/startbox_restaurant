package startbox.restaurant.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddOrderOption {

    @SerializedName("posale_id")
    private int posale_id;

    @SerializedName("store_id")
    private int store_id;

    @SerializedName("register_id")
    private int register_id;

    @SerializedName("table_id")
    private int table_id;

    @SerializedName("options")
    private List<AddOrderOptionDetail> options;

    @SerializedName("option_text")
    private String option_text;

    public int getPosale_id() {
        return posale_id;
    }

    public void setPosale_id(int posale_id) {
        this.posale_id = posale_id;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }

    public int getRegister_id() {
        return register_id;
    }

    public void setRegister_id(int register_id) {
        this.register_id = register_id;
    }

    public int getTable_id() {
        return table_id;
    }

    public void setTable_id(int table_id) {
        this.table_id = table_id;
    }

    public List<AddOrderOptionDetail> getOptions() {
        return options;
    }

    public void setOptions(List<AddOrderOptionDetail> options) {
        this.options = options;
    }

    public String getOption_text() {
        return option_text;
    }

    public void setOption_text(String option_text) {
        this.option_text = option_text;
    }
}
