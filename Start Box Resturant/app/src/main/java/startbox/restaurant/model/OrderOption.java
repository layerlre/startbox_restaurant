package startbox.restaurant.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class OrderOption {

    @SerializedName("id")
    private int id;

    @SerializedName("optionname")
    private String optionname;

    @SerializedName("optionprice")
    private int optionprice;

    @SerializedName("qty")
    private int qty;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOptionname() {
        return optionname;
    }

    public void setOptionname(String optionname) {
        this.optionname = optionname;
    }

    public int getOptionprice() {
        return optionprice;
    }

    public void setOptionprice(int optionprice) {
        this.optionprice = optionprice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
