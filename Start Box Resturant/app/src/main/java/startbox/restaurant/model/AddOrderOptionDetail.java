package startbox.restaurant.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddOrderOptionDetail {

    @SerializedName("option_id")
    private int option_id;

    @SerializedName("qty")
    private int qty;

    public int getOption_id() {
        return option_id;
    }

    public void setOption_id(int option_id) {
        this.option_id = option_id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
