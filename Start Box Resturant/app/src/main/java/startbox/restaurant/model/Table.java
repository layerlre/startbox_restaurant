package startbox.restaurant.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class Table {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("status")
    private int status;

    @SerializedName("time")
    private String time;

    @SerializedName("service")
    private int service;

    @SerializedName("tablemix_id")
    private int tablemix_id;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getStatus() {
        return status;
    }

    public String getTime() {
        return time;
    }

    public int getService() {
        return service;
    }

    public int getTablemixId() {
        return tablemix_id;
    }
}
