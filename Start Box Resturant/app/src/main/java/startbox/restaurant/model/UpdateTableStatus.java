package startbox.restaurant.model;

import com.google.gson.annotations.SerializedName;

public class UpdateTableStatus {
    @SerializedName("id")
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
