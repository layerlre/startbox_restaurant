package startbox.restaurant.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class KeepOrderDetail {


    @SerializedName("product_id")
    private int product_id;

    @SerializedName("order_qty")
    private int order_qty;

    public int getProductId() {
        return product_id;
    }

    public void setProductId(int product_id) {
        this.product_id = product_id;
    }

    public int getOrderQty() {
        return order_qty;
    }

    public void setOrderQty(int order_qty) {
        this.order_qty = order_qty;
    }
}
