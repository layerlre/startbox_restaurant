package startbox.restaurant.model;

import com.google.gson.annotations.SerializedName;

public class SubmitOrderPerTable {

    @SerializedName("table_id")
    private int table_id;

    @SerializedName("user_id")
    private int user_id;

    public int getTable_id() {
        return table_id;
    }

    public void setTable_id(int id) {
        this.table_id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
