package startbox.restaurant.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class Product {

    @SerializedName("id")
    private int id;

    @SerializedName("code")
    private String code;

    @SerializedName("name")
    private String name;

    @SerializedName("category")
    private String category;

    @SerializedName("cost")
    private Double cost;

    @SerializedName("price")
    private Double price;

    @SerializedName("photo")
    private String photo;

    @SerializedName("photothumb")
    private String photothumb;

    @SerializedName("type")
    private String type;

    @SerializedName("qty")
    private int qty;

    @SerializedName("order_qty")
    private int orderQty = 0;

    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public Double getCost() {
        return cost;
    }

    public Double getPrice() {
        return price;
    }

    public String getPhoto() {
        return photo;
    }

    public String getPhotothumb() {
        return photothumb;
    }

    public String getType() {
        return type;
    }

    public int getQty() {
        return qty;
    }

    public int getOrderQty() {
        return orderQty;
    }

    public void setOrderQty(int orderQty) {
        this.orderQty = orderQty;
    }
}
