package startbox.restaurant.model;

import com.google.gson.annotations.SerializedName;

public class SwapTable {
    @SerializedName("old_table_id")
    private int old_table_id;

    @SerializedName("new_table_id")
    private int new_table_id;

    public int getOldTableId() {
        return old_table_id;
    }

    public void setOldTableId(int old_table_id) {
        this.old_table_id = old_table_id;
    }

    public int getNewTableId() {
        return new_table_id;
    }

    public void setNewTableId(int new_table_id) {
        this.new_table_id = new_table_id;
    }
}
