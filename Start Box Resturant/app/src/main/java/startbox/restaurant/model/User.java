package startbox.restaurant.model;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id")
    private int id;

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("lastname")
    private String lastname;

    @SerializedName("avatar")
    private String avatar;

    @SerializedName("store_id")
    private int store_id;

    @SerializedName("register_id")
    private int register_id;

    @SerializedName("store_name")
    private String store_name;


    public int getId() {
        return id;
    }


    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getAvatar() {
        return avatar;
    }

    public int getStoreId() {
        return store_id;
    }

    public int getRegisterID() {
        return register_id;
    }

    public String getStoreName() {
        return store_name;
    }
}
