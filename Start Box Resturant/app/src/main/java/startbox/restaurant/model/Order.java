package startbox.restaurant.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class Order {

    @SerializedName("id")
    private int id;

    @SerializedName("product_id")
    private int product_id;

    @SerializedName("name")
    private String name;

    @SerializedName("options")
    private String options;

    @SerializedName("qt")
    private int qt;

    @SerializedName("photothumb")
    private String photothumb;

    @SerializedName("cookstatus")
    private int cookstatus;

    @SerializedName("table_id")
    private int table_id;

    @SerializedName("product_options")
    private List<OrderOption> orderOptions;

    public int getId() {
        return id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public String getName() {
        return name;
    }

    public String getOptions() {
        return options;
    }

    public int getQt() {
        return qt;
    }

    public String getPhotothumb() {
        return photothumb;
    }

    public int getCookstatus() {
        return cookstatus;
    }

    public List<OrderOption> getOrderOptions() {
        return orderOptions;
    }

    public int getTable_id() {
        return table_id;
    }
}
