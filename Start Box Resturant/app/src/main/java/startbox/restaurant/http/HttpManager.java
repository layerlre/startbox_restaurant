package startbox.restaurant.http;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import startbox.restaurant.http.service.AuthService;
import startbox.restaurant.http.service.OrderService;
import startbox.restaurant.http.service.ProductService;
import startbox.restaurant.http.service.TableService;
import startbox.restaurant.model.User;
import startbox.restaurant.util.Constant;
import startbox.restaurant.util.PreferenceManager;


public class HttpManager {

    private static HttpManager instance;

    public static HttpManager newInstance(String baseUrl) {
        PreferenceManager.getInstance().setBaseUrl(baseUrl);
        instance = new HttpManager();
        return instance;
    }

    public static HttpManager getInstance() {
        if (instance == null)
            instance = new HttpManager();
        return instance;
    }

    private HttpManager() {
    }

    private Retrofit createRequest() {
        Gson gson = new GsonBuilder()
                .setDateFormat(Constant.JSON_DATE_TIME_FORMAT)
                .create();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        httpClient.connectTimeout(60, TimeUnit.SECONDS);
//        httpClient.readTimeout(60, TimeUnit.SECONDS);
//        httpClient.writeTimeout(60, TimeUnit.SECONDS);
//        httpClient.retryOnConnectionFailure(false);
        httpClient.addInterceptor(logging);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder builder = original.newBuilder();

                User user = PreferenceManager.getInstance().getUser();
                if (user != null) {
                    builder.addHeader("UserID", String.valueOf(user.getId()));
                }

                builder = builder.method(original.method(), original.body());
                return chain.proceed(builder.build());
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(PreferenceManager.getInstance().getBaseUrl() )
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
        return retrofit;
    }

    private AuthService authService;
    private ProductService productService;
    private TableService tableService;
    private OrderService orderService;

    public AuthService getAuthService() {
        if (authService == null) {
            authService = this.createRequest().create(AuthService.class);
        }
        return authService;
    }


    public ProductService getProductService() {
        if (productService == null) {
            productService = this.createRequest().create(ProductService.class);
        }
        return productService;
    }

    private void createProductService() {

    }

    public TableService getTableService() {
        if (tableService == null) {
            tableService = this.createRequest().create(TableService.class);
        }
        return tableService;
    }

    public OrderService getOrderService() {
        if (orderService == null) {
            orderService = this.createRequest().create(OrderService.class);
        }
        return orderService;
    }

}

