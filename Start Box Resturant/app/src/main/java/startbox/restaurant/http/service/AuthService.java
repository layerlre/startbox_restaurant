package startbox.restaurant.http.service;

import io.reactivex.Flowable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import startbox.restaurant.model.User;
import startbox.restaurant.model.UserAuthen;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public interface AuthService {

    @GET("api/ping")
    Flowable<Response<String>> ping();

    @POST("api/auth/login")
    Flowable<Response<User>> login(@Body UserAuthen userAuthen);
}
