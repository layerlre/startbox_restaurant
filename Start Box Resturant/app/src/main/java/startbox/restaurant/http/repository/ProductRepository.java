package startbox.restaurant.http.repository;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DefaultSubscriber;
import retrofit2.Response;
import startbox.restaurant.http.HttpCallback;
import startbox.restaurant.http.HttpManager;
import startbox.restaurant.http.service.ProductService;
import startbox.restaurant.model.Category;
import startbox.restaurant.model.ProductCategory;
import startbox.restaurant.util.Util;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class ProductRepository {
    ProductService service;

    public ProductRepository() {
        service = HttpManager.getInstance().getProductService();
    }

    public void getCategory(final HttpCallback<List<Category>> httpCallback) {
        Flowable<Response<List<Category>>> register = service.getCategory();
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<List<Category>>>() {
                    @Override
                    public void onNext(Response<List<Category>> response) {
                        if (response.isSuccessful()) {
                            List<Category> resp = response.body();
                            httpCallback.OnSuccess(resp);
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getProduct(int storeID, final HttpCallback<List<ProductCategory>> httpCallback) {
        Flowable<Response<List<ProductCategory>>> register = service.getProduct(storeID);
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<List<ProductCategory>>>() {
                    @Override
                    public void onNext(Response<List<ProductCategory>> response) {
                        if (response.isSuccessful()) {
                            List<ProductCategory> resp = response.body();
                            httpCallback.OnSuccess(resp);
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
