package startbox.restaurant.http.service;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import startbox.restaurant.model.AddOrderOption;
import startbox.restaurant.model.Order;
import startbox.restaurant.model.Product;
import startbox.restaurant.model.SubmitOrderPerItem;
import startbox.restaurant.model.SubmitOrderPerTable;
import startbox.restaurant.model.KeepOrder;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public interface OrderService {


    @GET("api/order/table/{id}")
    Flowable<Response<List<Order>>> getOrder(@Path("id") int id);


    @POST("api/order/option")
    Flowable<Response<String>> addOption(@Body AddOrderOption option);


    @POST("api/order/keep")
    Flowable<Response<List<Product>>> keepOrder(@Body KeepOrder keepOrder);

    @DELETE("api/order/delete/{id}")
    Flowable<Response<String>> delete(@Path("id") int id);

    @POST("api/order/submit")
    Flowable<Response<String>> submit(@Body SubmitOrderPerItem submitOrderPerItem);

    @POST("api/order/submit/all")
    Flowable<Response<String>> submitAll(@Body SubmitOrderPerTable submitOrderPerTable);
}
