package startbox.restaurant.http.service;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import startbox.restaurant.model.SwapTable;
import startbox.restaurant.model.Table;
import startbox.restaurant.model.UpdateTableStatus;
import startbox.restaurant.model.Zone;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public interface TableService {

    @GET("api/table/zone/{storeId}")
    Flowable<Response<List<Zone>>> getZone(@Path("storeId") int storeId);

    @GET("api/table/{zoneId}")
    Flowable<Response<List<Table>>> getTable(@Path("zoneId") int zoneId);

    @POST("api/table/reserve")
    Flowable<Response<String>> reserve(@Body UpdateTableStatus updateTableStatus);

    @POST("api/table/close")
    Flowable<Response<String>> close(@Body UpdateTableStatus updateTableStatus);

    @POST("api/table/swap")
    Flowable<Response<String>> swap(@Body SwapTable swapTable);

}
