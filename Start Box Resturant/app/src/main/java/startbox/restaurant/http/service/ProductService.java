package startbox.restaurant.http.service;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import startbox.restaurant.model.Category;
import startbox.restaurant.model.Product;
import startbox.restaurant.model.ProductCategory;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public interface ProductService {


    @GET("api/product/category")
    Flowable<Response<List<Category>>> getCategory();

    @GET("api/product/{storeID}")
    Flowable<Response<List<ProductCategory>>> getProduct(
            @Path("storeID") int storeID
    );
}
