package startbox.restaurant.http.repository;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DefaultSubscriber;
import retrofit2.Response;
import startbox.restaurant.http.HttpCallback;
import startbox.restaurant.http.HttpManager;
import startbox.restaurant.http.service.AuthService;
import startbox.restaurant.model.User;
import startbox.restaurant.model.UserAuthen;
import startbox.restaurant.util.Util;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class AuthRepository {
    AuthService service;

    public AuthRepository() {
        service = HttpManager.getInstance().getAuthService();
    }

    public void ping(final HttpCallback<String> httpCallback) {
        Flowable<Response<String>> register = service.ping();
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<String>>() {
                    @Override
                    public void onNext(Response<String> response) {
                        if (response.isSuccessful()) {
                            String resp = response.body();
                            httpCallback.OnSuccess(resp);
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    public void authen(UserAuthen userAuthen, final HttpCallback<User> httpCallback) {
        Flowable<Response<User>> register = service.login(userAuthen);
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<User>>() {
                    @Override
                    public void onNext(Response<User> response) {
                        if (response.isSuccessful()) {
                            User resp = response.body();
                            httpCallback.OnSuccess(resp);
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


}
