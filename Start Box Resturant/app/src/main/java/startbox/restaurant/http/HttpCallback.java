package startbox.restaurant.http;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public interface HttpCallback<T> {
    void OnSuccess(T response);
    void OnError(String message);
}
