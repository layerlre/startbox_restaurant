package startbox.restaurant.http.repository;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DefaultSubscriber;
import retrofit2.Response;
import startbox.restaurant.http.HttpCallback;
import startbox.restaurant.http.HttpManager;
import startbox.restaurant.http.service.OrderService;
import startbox.restaurant.model.AddOrderOption;
import startbox.restaurant.model.Order;
import startbox.restaurant.model.Product;
import startbox.restaurant.model.SubmitOrderPerItem;
import startbox.restaurant.model.SubmitOrderPerTable;
import startbox.restaurant.model.KeepOrder;
import startbox.restaurant.util.Util;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class OrderRepository {
    OrderService service;

    public OrderRepository() {
        service = HttpManager.getInstance().getOrderService();
    }

    public void getOrder(int id, final HttpCallback<List<Order>> httpCallback) {
        Flowable<Response<List<Order>>> register = service.getOrder(id);
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<List<Order>>>() {
                    @Override
                    public void onNext(Response<List<Order>> response) {
                        if (response.isSuccessful()) {
                            List<Order> resp = response.body();
                            httpCallback.OnSuccess(resp);
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void addOption(AddOrderOption option, final HttpCallback<String> httpCallback) {
        Flowable<Response<String>> register = service.addOption(option);
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<String>>() {
                    @Override
                    public void onNext(Response<String> response) {
                        if (response.isSuccessful()) {
                            httpCallback.OnSuccess(response.body());
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void keepOrder(KeepOrder keepOrder, final HttpCallback<List<Product>> httpCallback) {
        Flowable<Response<List<Product>>> register = service.keepOrder(keepOrder);
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<List<Product>>>() {
                    @Override
                    public void onNext(Response<List<Product>> response) {
                        if (response.isSuccessful()) {
                            httpCallback.OnSuccess(response.body());
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void delete(int id, final HttpCallback<String> httpCallback) {
        Flowable<Response<String>> register = service.delete(id);
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<String>>() {
                    @Override
                    public void onNext(Response<String> response) {
                        if (response.isSuccessful()) {
                            httpCallback.OnSuccess(response.body());
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void submitOrderPerItem(SubmitOrderPerItem submitOrderPerItem, final HttpCallback<String> httpCallback) {
        Flowable<Response<String>> register = service.submit(submitOrderPerItem);
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<String>>() {
                    @Override
                    public void onNext(Response<String> response) {
                        if (response.isSuccessful()) {
                            httpCallback.OnSuccess(response.body());
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void submitAll(SubmitOrderPerTable submitOrderPerTable, final HttpCallback<String> httpCallback) {
        Flowable<Response<String>> register = service.submitAll(submitOrderPerTable);
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<String>>() {
                    @Override
                    public void onNext(Response<String> response) {
                        if (response.isSuccessful()) {
                            httpCallback.OnSuccess(response.body());
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
