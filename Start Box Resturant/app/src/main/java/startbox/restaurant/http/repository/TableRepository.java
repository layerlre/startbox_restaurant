package startbox.restaurant.http.repository;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DefaultSubscriber;
import retrofit2.Response;
import startbox.restaurant.http.HttpCallback;
import startbox.restaurant.http.HttpManager;
import startbox.restaurant.http.service.TableService;
import startbox.restaurant.model.SwapTable;
import startbox.restaurant.model.Table;
import startbox.restaurant.model.UpdateTableStatus;
import startbox.restaurant.model.Zone;
import startbox.restaurant.util.Util;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class TableRepository {
    TableService service;

    public TableRepository() {
        service = HttpManager.getInstance().getTableService();
    }


    public void getZone(int storeId, final HttpCallback<List<Zone>> httpCallback) {
        Flowable<Response<List<Zone>>> register = service.getZone(storeId);
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<List<Zone>>>() {
                    @Override
                    public void onNext(Response<List<Zone>> response) {
                        if (response.isSuccessful()) {
                            List<Zone> resp = response.body();
                            httpCallback.OnSuccess(resp);
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getTable(int zoneId, final HttpCallback<List<Table>> httpCallback) {
        Flowable<Response<List<Table>>> register = service.getTable(zoneId);
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<List<Table>>>() {
                    @Override
                    public void onNext(Response<List<Table>> response) {
                        if (response.isSuccessful()) {
                            List<Table> resp = response.body();
                            httpCallback.OnSuccess(resp);
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void reserve(int id, final HttpCallback<String> httpCallback) {

        UpdateTableStatus status = new UpdateTableStatus();
        status.setId(id);
        Flowable<Response<String>> register = service.reserve(status);
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<String>>() {
                    @Override
                    public void onNext(Response<String> response) {
                        if (response.isSuccessful()) {
                            String resp = response.body();
                            httpCallback.OnSuccess(resp);
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void close(int id, final HttpCallback<String> httpCallback) {

        UpdateTableStatus status = new UpdateTableStatus();
        status.setId(id);
        Flowable<Response<String>> register = service.close(status);
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<String>>() {
                    @Override
                    public void onNext(Response<String> response) {
                        if (response.isSuccessful()) {
                            String resp = response.body();
                            httpCallback.OnSuccess(resp);
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public void swap(SwapTable swapTable, final HttpCallback<String> httpCallback) {

        Flowable<Response<String>> register = service.swap(swapTable);
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<String>>() {
                    @Override
                    public void onNext(Response<String> response) {
                        if (response.isSuccessful()) {
                            String resp = response.body();
                            httpCallback.OnSuccess(resp);
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
