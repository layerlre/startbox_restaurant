package startbox.restaurant;

import android.app.Application;

import startbox.restaurant.util.Contextor;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


/**
 * Created by chawdee on 20/04/2016.
 */
public class MainApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		// Initialize thing(s) here
		Contextor.getInstance().init(getApplicationContext());
		initFont();
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
	}

	private void initFont() {
		CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
				.setDefaultFontPath("fonts/Superspace Light ver 1.00.otf")
				.setFontAttrId(R.attr.fontPath)
				.build()
		);
	}
}
