package startbox.restaurant.view.list_item;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import startbox.restaurant.R;
import startbox.restaurant.model.Category;
import startbox.restaurant.view.customview.BaseCustomViewGroup;

public class FoodTypeListItem extends BaseCustomViewGroup {

    TextView tvTitle;
    LinearLayout listItem;

    private Category item;

    public FoodTypeListItem(Context context) {
        super(context);
        initInflate();
        initInstances();
    }

    public FoodTypeListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInflate();
        initInstances();
        initWithAttrs(attrs, 0, 0);
    }

    public FoodTypeListItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initInflate();
        initInstances();
        initWithAttrs(attrs, defStyleAttr, 0);
    }

    @TargetApi(21)
    public FoodTypeListItem(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initInflate();
        initInstances();
        initWithAttrs(attrs, defStyleAttr, defStyleRes);
    }

    private void initInflate() {
        inflate(getContext(), R.layout.list_view_food_type, this);
    }

    private void initInstances() {
        tvTitle = findViewById(R.id.tv_title);
        listItem = findViewById(R.id.list_item);
    }

    private void initWithAttrs(AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        /*
        TypedArray a = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.StyleableName,
                defStyleAttr, defStyleRes);

        try {

        } finally {
            a.recycle();
        }
        */
    }

    public void setItem(Category item) {
        this.item = item;
        init();
    }

    public void init() {
        tvTitle.setText(this.item.getName());
    }

    public void setItemClickListener(View.OnClickListener itemClickListener) {
        listItem.setOnClickListener(itemClickListener);
    }
}