package startbox.restaurant.ui.order;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import startbox.restaurant.R;
import startbox.restaurant.adaptor.ListClickListener;
import startbox.restaurant.adaptor.OrderRecyclerViewAdapter;
import startbox.restaurant.http.HttpCallback;
import startbox.restaurant.http.repository.OrderRepository;
import startbox.restaurant.http.repository.TableRepository;
import startbox.restaurant.model.KeepOrder;
import startbox.restaurant.model.KeepOrderDetail;
import startbox.restaurant.model.Product;
import startbox.restaurant.model.Order;
import startbox.restaurant.model.SubmitOrderPerItem;
import startbox.restaurant.model.SubmitOrderPerTable;
import startbox.restaurant.model.Table;
import startbox.restaurant.model.User;
import startbox.restaurant.ui.BaseActivity;
import startbox.restaurant.ui.table.TableActivity;
import startbox.restaurant.util.PreferenceManager;
import startbox.restaurant.util.RequestCode;
import startbox.restaurant.util.Util;

public class OrderActivity extends BaseActivity {

    @BindView(R.id.btn_back)
    ImageButton btnBack;
    @BindView(R.id.tv_table_name)
    TextView tvTableName;
    @BindView(R.id.btn_close)
    ImageButton btnClose;
    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.btn_keep_order)
    LinearLayout btnKeepOrder;
    @BindView(R.id.btn_submit_order)
    LinearLayout btnSubmitOrder;

    Table table;
    OrderRecyclerViewAdapter adaptor;

    List<Order> orders;

    TableRepository tableRepository;
    private OrderRepository orderRepositiry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        ButterKnife.bind(this);

        init();
    }

    private void init() {

        table = Parcels.unwrap(getIntent().getParcelableExtra("Table"));
        tvTableName.setText(table.getName());
        orders = new ArrayList<>();
        tableRepository = new TableRepository();
        orderRepositiry = new OrderRepository();
        btnSubmitOrder.setVisibility(View.GONE);

        if (table.getStatus() == 0) {

            showProgressDialog();

            tableRepository.reserve(table.getId(), new HttpCallback<String>() {
                @Override
                public void OnSuccess(String response) {
                    hideProgressDialog();
                }

                @Override
                public void OnError(String message) {
                    Util.showToast(message);
                    hideProgressDialog();
                }
            });
        } else {
            getOrder();
        }

        initEvent();

    }

    private void getOrder() {
        showProgressDialog();
        orderRepositiry.getOrder(table.getId(), new HttpCallback<List<Order>>() {
            @Override
            public void OnSuccess(List<Order> orders) {
                hideProgressDialog();

                adaptor = new OrderRecyclerViewAdapter(orders);
                adaptor.setOnClickListener(new ListClickListener<Order>() {
                    @Override
                    public void onClick(Order item) {
                        Intent intent = new Intent(OrderActivity.this, OrderDialogActivity.class);
                        intent.putExtra("Order", Parcels.wrap(item));
                        startActivityForResult(intent, RequestCode.ORDER_OPTION);
                    }
                });

                adaptor.setOnRemoveListener(new ListClickListener<Order>() {
                    @Override
                    public void onClick(final Order item) {
                        remove(item);
                    }
                });
                adaptor.setPrintItemListener(new ListClickListener<Order>() {
                    @Override
                    public void onClick(final Order item) {
                        print(item);
                    }
                });
                recyclerView.setLayoutManager(new LinearLayoutManager(OrderActivity.this));
                recyclerView.setAdapter(adaptor);

                boolean isShowSubmit = false;
                for (Order o : orders) {
                    if (o.getCookstatus() == 1) {
                        isShowSubmit = true;
                    }
                }

                btnSubmitOrder.setVisibility(isShowSubmit ? View.VISIBLE : View.GONE);
            }

            @Override
            public void OnError(String message) {
                handleError(message);
            }
        });
    }

    private void remove(final Order order) {
        showConfirm(getString(R.string.confirm_delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showProgressDialog();
                orderRepositiry.delete(order.getId(), new HttpCallback<String>() {
                    @Override
                    public void OnSuccess(String response) {
                        getOrder();
                    }

                    @Override
                    public void OnError(String message) {
                        handleError(message);
                    }
                });
            }
        });
    }

    private void print(final Order order) {
        showConfirm(getString(R.string.print), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showProgressDialog();

                SubmitOrderPerItem submitOrderPerItem = new SubmitOrderPerItem();
                submitOrderPerItem.setId(order.getId());
                submitOrderPerItem.setUser_id(PreferenceManager.getInstance().getUser().getId());

                orderRepositiry.submitOrderPerItem(submitOrderPerItem, new HttpCallback<String>() {
                    @Override
                    public void OnSuccess(String response) {
                        getOrder();
                    }

                    @Override
                    public void OnError(String message) {
                        handleError(message);
                    }
                });
            }
        });
    }

    private void printAll() {
        showConfirm(getString(R.string.confirm_order), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SubmitOrderPerTable submitOrderPerTable = new SubmitOrderPerTable();
                submitOrderPerTable.setTable_id(table.getId());
                submitOrderPerTable.setUser_id(PreferenceManager.getInstance().getUser().getId());

                showProgressDialog();
                orderRepositiry.submitAll(submitOrderPerTable, new HttpCallback<String>() {
                    @Override
                    public void OnSuccess(String response) {
                        getOrder();
                    }

                    @Override
                    public void OnError(String message) {
                        handleError(message);
                    }
                });
            }
        });
    }


    private void initEvent() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(OrderActivity.this);
                builder.setCancelable(true);
                builder.setMessage(getString(R.string.cancel_table));
                builder.setPositiveButton(getString(R.string.confirm),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                showProgressDialog();
                                tableRepository.close(table.getId(), new HttpCallback<String>() {
                                    @Override
                                    public void OnSuccess(String response) {
                                        hideProgressDialog();
                                        onBackPressed();
                                    }

                                    @Override
                                    public void OnError(String message) {
                                        Util.showToast(message);
                                        hideProgressDialog();
                                    }
                                });
                            }
                        });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        btnKeepOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OrderActivity.this, ProductListActivity.class);
                intent.putExtra("Table", Parcels.wrap(table));
                startActivityForResult(intent, RequestCode.FOOD_SELECTOR);
            }
        });

        btnSubmitOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                printAll();
            }
        });

        tvTableName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(OrderActivity.this);
                builder.setCancelable(true);
                builder.setMessage(getString(R.string.change_table));
                builder.setPositiveButton(getString(R.string.confirm),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(OrderActivity.this, TableActivity.class);
                                intent.putExtra("OldTable", Parcels.wrap(table));
                                startActivityForResult(intent, RequestCode.SWAP_TABLE);
                            }
                        });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });

    }

    private void handleError(String message) {
        hideProgressDialog();
        if (message.equals("INVALID_HOLD")) {
            Intent intent = new Intent(this, TableActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("Error", true);
            startActivity(intent);
        } else {
            Util.showToast(message);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RequestCode.FOOD_SELECTOR:

                    User user = PreferenceManager.getInstance().getUser();

                    List<KeepOrderDetail> details = Parcels.unwrap(data.getParcelableExtra("Detail"));

                    KeepOrder keepOrder = new KeepOrder();
                    keepOrder.setDetails(details);
                    keepOrder.setRegister_id(user.getRegisterID());
                    keepOrder.setUser_id(user.getId());
                    keepOrder.setTable_id(table.getId());
                    keepOrder.setStore_id(PreferenceManager.getInstance().getUser().getStoreId());

                    showProgressDialog();
                    orderRepositiry.keepOrder(keepOrder, new HttpCallback<List<Product>>() {
                        @Override
                        public void OnSuccess(List<Product> response) {

                            getOrder();

                            if (response.size() > 0) {
                                Intent intent = new Intent(OrderActivity.this, OutOfStockActivity.class);
                                intent.putExtra("Product", Parcels.wrap(response));
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void OnError(String message) {
                            handleError(message);
                        }
                    });

                    break;
                case RequestCode.ORDER_OPTION:
                    getOrder();
                    break;

                case RequestCode.SWAP_TABLE:
                    table = Parcels.unwrap(data.getParcelableExtra("Table"));
                    tvTableName.setText(table.getName());
                    break;
            }
        }

    }

}
