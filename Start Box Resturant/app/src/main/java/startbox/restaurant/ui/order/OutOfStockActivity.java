package startbox.restaurant.ui.order;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import startbox.restaurant.R;
import startbox.restaurant.adaptor.OutOfStockRecyclerViewAdapter;
import startbox.restaurant.model.Product;
import startbox.restaurant.ui.BaseActivity;

public class OutOfStockActivity extends BaseActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out_of_stock);
        ButterKnife.bind(this);


        List<Product> products = Parcels.unwrap(getIntent().getParcelableExtra("Product"));

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        OutOfStockRecyclerViewAdapter adapter = new OutOfStockRecyclerViewAdapter(products);
        recyclerView.setAdapter(adapter);

    }
}
