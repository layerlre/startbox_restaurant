package startbox.restaurant.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import startbox.restaurant.R;
import startbox.restaurant.http.HttpCallback;
import startbox.restaurant.http.HttpManager;
import startbox.restaurant.http.repository.AuthRepository;
import startbox.restaurant.http.repository.TableRepository;
import startbox.restaurant.ui.BaseActivity;
import startbox.restaurant.util.LocaleHelper;
import startbox.restaurant.util.PreferenceManager;
import startbox.restaurant.util.Util;


public class CheckEndPointActivity extends BaseActivity {

    @BindView(R.id.tv_url)
    EditText tvUrl;
    @BindView(R.id.btn_check)
    Button btnCheck;
    @BindView(R.id.switch_lang)
    Switch switchLang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_end_point);
        ButterKnife.bind(this);

        if (PreferenceManager.getInstance().getBaseUrl() != null) {
            checkEndPoint();
        }

        btnCheck.setOnClickListener(onClickListener);

        switchLang.setChecked(LocaleHelper.getPersistedData(this, LocaleHelper.THAI_LANGUAGE).equals(LocaleHelper.ENGLISH_LANGUAGE));

        switchLang.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateView(isChecked ? LocaleHelper.ENGLISH_LANGUAGE : LocaleHelper.THAI_LANGUAGE);
                recreate();
            }
        });

        tvUrl.setText(PreferenceManager.getInstance().getBaseUrl());
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (tvUrl.getText().toString().length() != 0) {

                String url = tvUrl.getText().toString();
                if (!url.startsWith("http://") && !url.startsWith("https://")) {
                    url = "http://" + url;
                }
                if (!url.endsWith("/")) {
                    url += "/";
                }

                HttpManager.newInstance(url);
                checkEndPoint();

                /*
                String URL_REGEX = "^((https?|ftp)://|(www|ftp)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$";
                Pattern p = Pattern.compile(URL_REGEX);
                Matcher m = p.matcher(url);

                if (m.find()) {
                    HttpManager.newInstance(url);
                    checkEndPoint();
                }else {
                    tvUrl.setError(getString(R.string.invalid_url));
                }*/

            } else {
                tvUrl.setError(getString(R.string.require_url));
            }


        }
    };

    private void checkEndPoint() {
        showProgressDialog();

        AuthRepository authRepository = new AuthRepository();
        authRepository.ping(new HttpCallback<String>() {
            @Override
            public void OnSuccess(String response) {

                hideProgressDialog();

                Intent intent = new Intent(CheckEndPointActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();

            }

            @Override
            public void OnError(String message) {
                hideProgressDialog();
                //PreferenceManager.getInstance().setBaseUrl(null);
                Util.showToast(getString(R.string.not_found_service));
            }
        });
    }
}
