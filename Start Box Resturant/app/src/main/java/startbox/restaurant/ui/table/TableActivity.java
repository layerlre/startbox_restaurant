package startbox.restaurant.ui.table;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.parceler.Parcels;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import startbox.restaurant.R;
import startbox.restaurant.http.HttpCallback;
import startbox.restaurant.http.repository.TableRepository;
import startbox.restaurant.model.SwapTable;
import startbox.restaurant.model.Table;
import startbox.restaurant.model.User;
import startbox.restaurant.model.Zone;
import startbox.restaurant.ui.BaseActivity;
import startbox.restaurant.ui.order.OrderActivity;
import startbox.restaurant.util.PreferenceManager;
import startbox.restaurant.util.Util;

public class TableActivity extends BaseActivity implements TableListFragment.OnFragmentInteractionListener {

    Table oldTable;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.btn_back)
    ImageButton btnBack;
    @BindView(R.id.layout_change_table)
    LinearLayout layoutChangeTable;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.contentContainer)
    FrameLayout contentContainer;

    private FragmentStatePagerAdapter pagerAdapter;

    private TableRepository commonRepository;
    private User user;
    private List<Zone> zones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);
        ButterKnife.bind(this);

        Parcelable parcelable = getIntent().getParcelableExtra("OldTable");
        if (parcelable != null) {
            oldTable = Parcels.unwrap(parcelable);
        }

        init();
    }

    private void init() {

        user = PreferenceManager.getInstance().getUser();
        commonRepository = new TableRepository();
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        layoutChangeTable.setVisibility(oldTable != null ? View.VISIBLE : View.GONE);

        getZone();

        if (getIntent().getBooleanExtra("Error", false)) {
            Util.showToast("Table is no reservations.");
        }

        for (int i = 0; i < tabs.getTabCount(); i++) {
            //noinspection ConstantConditions
            TextView tv = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab,null);
            //tv.setTypeface(Typeface);
            tabs.getTabAt(i).setCustomView(tv);
        }
    }

    private void getZone() {

        showProgressDialog();
        commonRepository.getZone(user.getStoreId(), new HttpCallback<List<Zone>>() {
            @Override
            public void OnSuccess(List<Zone> response) {
                hideProgressDialog();
                zones = response;

                pagerAdapter = getFragmentStatePagerAdapter();
                viewpager.setAdapter(pagerAdapter);

                tabs.setupWithViewPager(viewpager);

                for (int i = 0; i < zones.size(); i++) {
                    Objects.requireNonNull(tabs.getTabAt(i)).setText(zones.get(i).getName());
                }

                Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Superspace Light ver 1.00.otf");
                ViewGroup vg = (ViewGroup) tabs.getChildAt(0);
                int tabsCount = vg.getChildCount();
                for (int j = 0; j < tabsCount; j++) {
                    ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
                    int tabChildsCount = vgTab.getChildCount();
                    for (int i = 0; i < tabChildsCount; i++) {
                        View tabViewChild = vgTab.getChildAt(i);
                        if (tabViewChild instanceof TextView) {
                            ((TextView) tabViewChild).setTypeface(font);
                            ((TextView) tabViewChild).setTextSize(25);

                        }
                    }
                }

            }

            @Override
            public void OnError(String message) {
                hideProgressDialog();
                Util.showToast(message);
            }
        });
    }


    @NonNull
    private FragmentStatePagerAdapter getFragmentStatePagerAdapter() {

        return new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return TableListFragment.newInstance(zones.get(position));
            }

            @Override
            public int getCount() {
                return zones.size();
            }
        };
    }

    @Override
    public void onItemClick(final Table table) {
        if (oldTable != null) {
            if (table.getStatus() == 1) {
                showAlert(getString(R.string.busy));
                return;
            } else {

                showConfirm(getString(R.string.change_table_confirm), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SwapTable swapTable = new SwapTable();
                        swapTable.setNewTableId(table.getId());
                        swapTable.setOldTableId(oldTable.getId());

                        showProgressDialog();
                        TableRepository tableRepository = new TableRepository();
                        tableRepository.swap(swapTable, new HttpCallback<String>() {
                            @Override
                            public void OnSuccess(String response) {
                                Intent intent = getIntent();
                                intent.putExtra("Table", Parcels.wrap(table));
                                setResult(RESULT_OK, intent);
                                Util.showToast(getString(R.string.change_table_success));
                                finish();
                            }

                            @Override
                            public void OnError(String message) {
                                if (message.equals("INVALID_HOLD")) {
                                    Intent intent = new Intent(TableActivity.this, TableActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.putExtra("Error", true);
                                    startActivity(intent);
                                } else {
                                    Util.showToast(message);
                                }
                                hideProgressDialog();
                            }
                        });
                    }
                });

            }
        } else {
            Intent intent = new Intent(TableActivity.this, OrderActivity.class);
            intent.putExtra("Table", Parcels.wrap(table));
            startActivity(intent);
        }

    }
}
