package startbox.restaurant.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import startbox.restaurant.R;
import startbox.restaurant.ui.BaseActivity;
import startbox.restaurant.ui.setup.SetupActivity;
import startbox.restaurant.ui.table.TableActivity;
import startbox.restaurant.view.customview.SquareLinerLayout;

public class MainActivity extends BaseActivity {


    @BindView(R.id.btn_table)
    SquareLinerLayout btnTable;
    @BindView(R.id.btn_setup)
    SquareLinerLayout btnSetup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        btnTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TableActivity.class);
                startActivity(intent);
            }
        });

        btnSetup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SetupActivity.class);
                startActivity(intent);
            }
        });

    }
}
