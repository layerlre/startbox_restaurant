package startbox.restaurant.ui.table;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import startbox.restaurant.adaptor.ListClickListener;
import startbox.restaurant.adaptor.TableRecyclerViewAdapter;
import startbox.restaurant.R;
import startbox.restaurant.http.HttpCallback;
import startbox.restaurant.http.repository.TableRepository;
import startbox.restaurant.model.Table;
import startbox.restaurant.model.Zone;
import startbox.restaurant.util.Util;


public class TableListFragment extends Fragment {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    Unbinder unbinder;

    private TableRecyclerViewAdapter adapter;
    private TableListFragment.OnFragmentInteractionListener listener;
    private TableRepository commonRepository;

    Zone zone;

    public TableListFragment() {
        // Required empty public constructor
    }

    public static TableListFragment newInstance(Zone zone) {
        TableListFragment fragment = new TableListFragment();
        Bundle args = new Bundle();
        args.putParcelable("Zone", Parcels.wrap(zone));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        zone = Parcels.unwrap(getArguments().getParcelable("Zone"));
        commonRepository = new TableRepository();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_table_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        initInstances(view, savedInstanceState);
        return view;
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {

        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onItemClick(Table table);
    }

    @Override
    public void onResume() {
        super.onResume();
        commonRepository.getTable(zone.getId(), new HttpCallback<List<Table>>() {
            @Override
            public void OnSuccess(List<Table> response) {
                adapter = new TableRecyclerViewAdapter(response);
                adapter.setOnClickListener(new ListClickListener<Table>() {
                    @Override
                    public void onClick(Table item) {
                        listener.onItemClick(item);
                    }
                });

                recyclerView.setAdapter(adapter);
            }

            @Override
            public void OnError(String message) {
                Util.showToast(message);
            }
        });
    }
}
