package startbox.restaurant.ui.setup;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import startbox.restaurant.R;
import startbox.restaurant.http.HttpCallback;
import startbox.restaurant.http.HttpManager;
import startbox.restaurant.http.repository.AuthRepository;
import startbox.restaurant.ui.BaseActivity;
import startbox.restaurant.ui.main.CheckEndPointActivity;
import startbox.restaurant.ui.main.LoginActivity;
import startbox.restaurant.util.LocaleHelper;
import startbox.restaurant.util.PreferenceManager;
import startbox.restaurant.util.Util;

public class SetupActivity extends BaseActivity {

    @BindView(R.id.btn_back)
    ImageButton btnBack;
    @BindView(R.id.app_bar)
    LinearLayout appBar;
    @BindView(R.id.et_url)
    EditText etUrl;
    @BindView(R.id.switch_lang)
    Switch switchLang;
    @BindView(R.id.btn_logout)
    Button btnLogout;
    @BindView(R.id.btn_save)
    Button btnSave;
    @BindView(R.id.tv_store_name)
    TextView tvStoreName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        ButterKnife.bind(this);

        etUrl.setText(PreferenceManager.getInstance().getBaseUrl());


        switchLang.setChecked(LocaleHelper.getPersistedData(this, LocaleHelper.THAI_LANGUAGE).equals(LocaleHelper.ENGLISH_LANGUAGE));

        switchLang.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateView(isChecked ? LocaleHelper.ENGLISH_LANGUAGE : LocaleHelper.THAI_LANGUAGE);
                recreate();
            }
        });

        tvStoreName.setText(PreferenceManager.getInstance().getUser().getStoreName());

    }

    @OnClick(R.id.btn_back)
    public void onBackClick() {
        onBackPressed();
    }

    @OnClick(R.id.btn_logout)
    public void onViewClicked() {

        AlertDialog.Builder builder = new AlertDialog.Builder(SetupActivity.this);
        builder.setCancelable(true);
        builder.setMessage(getString(R.string.logout));
        builder.setPositiveButton(getString(R.string.confirm),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finishAffinity();

                        Intent intent = new Intent(SetupActivity.this, LoginActivity.class);
                        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                    }
                });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
        PreferenceManager.getInstance().setUser(null);

    }

    @OnClick(R.id.btn_save)
    public void onSaveClick() {
        if (etUrl.getText().toString().length() != 0) {

            if (Patterns.WEB_URL.matcher(etUrl.getText().toString()).matches()) {
                etUrl.setError(null);
                String url = etUrl.getText().toString();


                if (!url.startsWith("http://") && !url.startsWith("https://")) {
                    url = "http://" + url;
                }
                if (!url.endsWith("/")) {
                    url += "/";
                }
                HttpManager.newInstance(url);
                checkEndPoint();
            } else {
                etUrl.setError(getString(R.string.invalid_url));
            }
        } else {
            etUrl.setError(getString(R.string.require_url));
        }
    }

    private void checkEndPoint() {
        showProgressDialog();

        AuthRepository authRepository = new AuthRepository();
        authRepository.ping(new HttpCallback<String>() {
            @Override
            public void OnSuccess(String response) {

                hideProgressDialog();

                Util.showToast(getString(R.string.success));

            }

            @Override
            public void OnError(String message) {
                hideProgressDialog();
                PreferenceManager.getInstance().setBaseUrl(null);
                Util.showToast(getString(R.string.not_found_service));
            }
        });
    }
}
