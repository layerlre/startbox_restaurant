
package startbox.restaurant.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import startbox.restaurant.R;
import startbox.restaurant.http.HttpCallback;
import startbox.restaurant.http.repository.AuthRepository;
import startbox.restaurant.http.repository.TableRepository;
import startbox.restaurant.model.User;
import startbox.restaurant.model.UserAuthen;
import startbox.restaurant.ui.BaseActivity;
import startbox.restaurant.util.LocaleHelper;
import startbox.restaurant.util.PreferenceManager;
import startbox.restaurant.util.Util;


public class LoginActivity extends BaseActivity {


    @BindView(R.id.et_user)
    EditText etUser;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;

    AuthRepository authRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        PreferenceManager.getInstance().setUser(null);

        authRepository = new AuthRepository();

        btnLogin.setOnClickListener(onClickListener);


    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            UserAuthen userAuthen = new UserAuthen();
            userAuthen.setUsername(etUser.getText().toString());
            userAuthen.setPassword(etPassword.getText().toString());

            showProgressDialog();
            authRepository.authen(userAuthen, userHttpCallback);
        }
    };

    private HttpCallback<User> userHttpCallback = new HttpCallback<User>() {
        @Override
        public void OnSuccess(User response) {
            hideProgressDialog();
            PreferenceManager.getInstance().setUser(response);
            gotoMainActivity();
        }

        @Override
        public void OnError(String message) {
            switch (message) {
                case "NF":

                    Util.showToast(getResources().getString(R.string.invalid_user));

                    break;
                case "NA":

                    Util.showToast(getResources().getString(R.string.not_permission));

                    break;

                case "SC":

                    Util.showToast(getResources().getString(R.string.store_close));

                    break;

                default:

                    Util.showToast(message);

                    break;

            }

            hideProgressDialog();
        }
    };

    public void gotoMainActivity() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
//        PreferenceManager.getInstance().setBaseUrl(null);
//        Intent intent = new Intent(LoginActivity.this, CheckEndPointActivity.class);
//        startActivity(intent);
        finish();
    }
}
