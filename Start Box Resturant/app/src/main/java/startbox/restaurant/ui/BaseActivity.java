package startbox.restaurant.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import org.parceler.Parcels;

import startbox.restaurant.R;
import startbox.restaurant.ui.order.OrderActivity;
import startbox.restaurant.ui.table.TableActivity;
import startbox.restaurant.util.DialogUtil;
import startbox.restaurant.util.LocaleHelper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends AppCompatActivity {

    private DialogUtil dialogUtil;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dialogUtil = new DialogUtil(this);

    }

    @Override
    protected void attachBaseContext(Context base) {
        base = LocaleHelper.onAttach(base, LocaleHelper.THAI_LANGUAGE);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    public void showProgressDialog(String message) {
        dialogUtil.showProgressDialog(message);
    }

    public void showProgressDialog() {
        dialogUtil.showProgressDialog(getString(R.string.loading));
    }

    public void hideProgressDialog() {
        dialogUtil.hideProgressDialog();
    }

    public void showAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });


        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showConfirm(String message, final DialogInterface.OnClickListener confirmListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        confirmListener.onClick(dialogInterface, i);
                        dialogInterface.dismiss();
                    }
                });
        builder.setNegativeButton(getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void updateView(String lang) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(LocaleHelper.IS_CHANGE_LANGUAGE, true);
        editor.apply();

        LocaleHelper.setLocale(this, lang);
    }
}
