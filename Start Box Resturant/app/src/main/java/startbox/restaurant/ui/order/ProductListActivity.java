package startbox.restaurant.ui.order;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import startbox.restaurant.R;
import startbox.restaurant.adaptor.ListClickListener;
import startbox.restaurant.adaptor.ProductListRecyclerViewAdapter;
import startbox.restaurant.http.HttpCallback;
import startbox.restaurant.http.repository.ProductRepository;
import startbox.restaurant.model.KeepOrderDetail;
import startbox.restaurant.model.Product;
import startbox.restaurant.model.ProductCategory;
import startbox.restaurant.model.Table;
import startbox.restaurant.ui.BaseActivity;
import startbox.restaurant.util.PreferenceManager;
import startbox.restaurant.util.Util;

public class ProductListActivity extends BaseActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.et_search)
    EditText etSearch;

    @BindView(R.id.btn_back)
    ImageButton btnBack;

    @BindView(R.id.tv_confirm)

    TextView tvConfirm;
    @BindView(R.id.tv_table_name)
    TextView tvTableName;

    ProductListRecyclerViewAdapter adapter;
    ProductRepository productRepository;

    List<ProductCategory> data = new ArrayList<>();
    List<ProductCategory> filter = new ArrayList<>();

    List<KeepOrderDetail> details = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        ButterKnife.bind(this);

        init();
    }

    private void init() {

        Table table = Parcels.unwrap(getIntent().getParcelableExtra("Table"));

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (details.size() > 0) {
                    Intent intent = getIntent();
                    intent.putExtra("Detail", Parcels.wrap(details));
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });

        productRepository = new ProductRepository();
        tvTableName.setText(table.getName());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        showProgressDialog();
        productRepository.getProduct(PreferenceManager.getInstance().getUser().getStoreId(), new HttpCallback<List<ProductCategory>>() {
            @Override
            public void OnSuccess(List<ProductCategory> response) {
                data = new ArrayList<>(response);
                filter = new ArrayList<>(response);
                adapter = new ProductListRecyclerViewAdapter(filter);
                adapter.setOnClickListener(new ListClickListener<List<Product>>() {
                    @Override
                    public void onClick(List<Product> item) {

                        details.clear();


                        int count = 0;
                        for (Product product : item) {
                            count += product.getOrderQty();
                            KeepOrderDetail detail = new KeepOrderDetail();
                            detail.setProductId(product.getId());
                            detail.setOrderQty(product.getOrderQty());
                            details.add(detail);
                        }
                        tvConfirm.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
                        tvConfirm.setText(String.valueOf(count));
                    }
                });

                recyclerView.setAdapter(adapter);

                etSearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        adapter.getFilter().filter(s);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                hideProgressDialog();
            }

            @Override
            public void OnError(String message) {
                Util.showToast(message);
                hideProgressDialog();
            }
        });

    }


}
