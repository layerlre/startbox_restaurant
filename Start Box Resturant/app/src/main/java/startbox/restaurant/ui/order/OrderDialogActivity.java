package startbox.restaurant.ui.order;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.parceler.Parcel;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import startbox.restaurant.R;
import startbox.restaurant.adaptor.ListClickListener;
import startbox.restaurant.adaptor.OrderOptionRecyclerViewAdapter;
import startbox.restaurant.http.HttpCallback;
import startbox.restaurant.http.repository.OrderRepository;
import startbox.restaurant.model.AddOrderOption;
import startbox.restaurant.model.AddOrderOptionDetail;
import startbox.restaurant.model.Order;
import startbox.restaurant.model.OrderOption;
import startbox.restaurant.model.User;
import startbox.restaurant.ui.BaseActivity;
import startbox.restaurant.ui.table.TableActivity;
import startbox.restaurant.util.PreferenceManager;
import startbox.restaurant.util.Util;

public class OrderDialogActivity extends BaseActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.et_detail)
    EditText etDetail;
    @BindView(R.id.btn_ok)
    Button btnOk;

    OrderOptionRecyclerViewAdapter adapter;

    List<OrderOption> selected;
    OrderRepository orderRepository = new OrderRepository();

    Order order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_dialog);
        ButterKnife.bind(this);

        order = Parcels.unwrap(getIntent().getParcelableExtra("Order"));


        init();
    }

    private void init() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new OrderOptionRecyclerViewAdapter(order.getOrderOptions());

        recyclerView.setAdapter(adapter);

        if (order.getCookstatus() != 1) {
            btnOk.setVisibility(View.GONE);
            etDetail.setEnabled(false);
            recyclerView.setEnabled(false);
        } else {
            adapter.setSelectedChange(new ListClickListener<List<OrderOption>>() {
                @Override
                public void onClick(List<OrderOption> item) {
                    selected = item;
                }
            });
        }

        selected = adapter.getSelectedItems();

        etDetail.setText(order.getOptions());

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                List<AddOrderOptionDetail> selected = new ArrayList<>();
                for (OrderOption s : OrderDialogActivity.this.selected) {
                    AddOrderOptionDetail detail = new AddOrderOptionDetail();
                    detail.setOption_id(s.getId());
                    detail.setQty(s.getQty());
                    selected.add(detail);
                }

                User user = PreferenceManager.getInstance().getUser();

                AddOrderOption addOrderOption = new AddOrderOption();
                addOrderOption.setOption_text(etDetail.getText().toString());
                addOrderOption.setPosale_id(order.getId());
                addOrderOption.setTable_id(order.getTable_id());
                addOrderOption.setOptions(selected);
                addOrderOption.setStore_id(user.getStoreId());
                addOrderOption.setRegister_id(user.getRegisterID());

                showProgressDialog();
                orderRepository.addOption(addOrderOption, new HttpCallback<String>() {
                    @Override
                    public void OnSuccess(String response) {
                        hideProgressDialog();
                        setResult(RESULT_OK);
                        finish();
                    }

                    @Override
                    public void OnError(String message) {
                        hideProgressDialog();
                        finish();
                        if (message.equals("INVALID_HOLD")) {
                            Intent intent = new Intent(OrderDialogActivity.this, TableActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("Error", true);
                            startActivity(intent);
                        } else {
                            Util.showToast(message);
                        }
                    }
                });


            }
        });


    }

}
