package startbox.restaurant.ui.order;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import startbox.restaurant.R;
import startbox.restaurant.ui.BaseActivity;

public class SubmitOrderActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_order);
    }
}
