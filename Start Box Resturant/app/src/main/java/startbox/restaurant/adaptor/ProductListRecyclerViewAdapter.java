package startbox.restaurant.adaptor;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import startbox.restaurant.R;
import startbox.restaurant.model.Product;
import startbox.restaurant.model.ProductCategory;


/**
 * Created by phitakphong on 17/05/2559.
 */
public class ProductListRecyclerViewAdapter extends RecyclerView.Adapter<ProductListRecyclerViewAdapter.ViewHolder> implements Filterable {

    private List<ProductCategory> filters;
    private List<ProductCategory> items;
    private LayoutInflater inflater;
    private ListClickListener<List<Product>> clickListener;
    private int expandedPosition = -1;

    public ProductListRecyclerViewAdapter(List<ProductCategory> items) {
        this.items = items;
        this.filters = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_product_list_item, parent, false);
        return new ViewHolder(itemView, parent.getContext());
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final boolean isExpanded = position == expandedPosition;

        final ProductCategory item = filters.get(position);
        holder.init(item);

        if (clickListener != null) {
            holder.setOnClickListener(new ListClickListener<Product>() {
                @Override
                public void onClick(Product item) {
                    List<Product> products = new ArrayList<>();
                    for (ProductCategory category : items) {
                        for (Product product : category.getProducts()) {
                            if (product.getOrderQty() > 0) {
                                products.add(product);
                            }
                        }
                    }
                    clickListener.onClick(products);
                }
            });
        }

        holder.toggleDetail(isExpanded);

        holder.setItemClick(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandedPosition = isExpanded ? -1 : position;
                notifyDataSetChanged();
            }
        });

    }

    public void setOnClickListener(ListClickListener<List<Product>> clickListener) {
        this.clickListener = clickListener;
    }


    @Override
    public int getItemCount() {
        return filters.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filters = items;
                } else {

                    List<ProductCategory> filteredList = new ArrayList<>();
                    for (ProductCategory item : items) {
                        ProductCategory filterCategory = new ProductCategory();
                        filterCategory.setId(item.getId());
                        filterCategory.setName(item.getName());

                        List<Product> products = new ArrayList<>();

                        for (Product product : item.getProducts()) {
                            if (product.getName().contains(charString) || product.getCode().contains(charString)) {
                                products.add(product);
                            }
                        }
                        if (products.size() > 0) {
                            filterCategory.setProducts(products);
                            filteredList.add(filterCategory);
                        }
                    }

                    filters = filteredList;

                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filters;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filters = (ArrayList<ProductCategory>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_icon)
        ImageView ivIcon;

        @BindView(R.id.tv_name)
        TextView tvName;

        @BindView(R.id.list_item)
        RelativeLayout listItem;

        @BindView(R.id.li_detail)
        LinearLayout liDetail;

        @BindView(R.id.recycler_view)
        RecyclerView recyclerView;

        Context context;

        private ListClickListener<Product> clickListener;

        public ViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            ButterKnife.bind(this, view);
        }

        private ProductRecyclerViewAdapter adapter;

        private void init(ProductCategory item) {
            tvName.setText(item.getName());

            adapter = new ProductRecyclerViewAdapter(item.getProducts());

            recyclerView.setLayoutManager(new GridLayoutManager(context, 3));
            adapter.setClickListener(clickListener);
            recyclerView.setAdapter(adapter);

        }

        private void toggleDetail(boolean isShow) {
            liDetail.setVisibility(isShow ? View.VISIBLE : View.GONE);
            ivIcon.setImageResource(isShow ? R.drawable.ic_arrow_down : R.drawable.ic_arrow_back);
        }

        public void setItemClick(View.OnClickListener itemClick) {
            listItem.setOnClickListener(itemClick);
        }

        public void setOnClickListener(ListClickListener<Product> clickListener) {
            this.clickListener = clickListener;
        }


    }
}