package startbox.restaurant.adaptor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import startbox.restaurant.R;
import startbox.restaurant.model.Order;
import startbox.restaurant.model.Product;
import startbox.restaurant.util.PreferenceManager;
import startbox.restaurant.util.Util;


/**
 * Created by phitakphong on 17/05/2559.
 */
public class OutOfStockRecyclerViewAdapter extends RecyclerView.Adapter<OutOfStockRecyclerViewAdapter.ViewHolder> {

    private List<Product> items;
    private LayoutInflater inflater;

    public OutOfStockRecyclerViewAdapter(List<Product> items) {
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_out_of_stock, parent, false);
        return new ViewHolder(itemView, parent.getContext());
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Product product = items.get(position);
        holder.init(product);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_food_icon)
        ImageView ivFoodIcon;

        @BindView(R.id.tv_food_name)
        TextView tvFoodName;

        private Context context;

        public ViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            ButterKnife.bind(this, view);
        }

        private void init(Product product) {

            Util.loadImageFromUrl(context, PreferenceManager.getInstance().getBaseUrl() + "/files/products/" + product.getPhotothumb(), ivFoodIcon);
            tvFoodName.setText(product.getName());
        }

    }
}