package startbox.restaurant.adaptor;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import startbox.restaurant.R;
import startbox.restaurant.model.Product;
import startbox.restaurant.model.ProductCategory;
import startbox.restaurant.util.PreferenceManager;
import startbox.restaurant.util.Util;
import startbox.restaurant.view.customview.SquareLinerLayout;


/**
 * Created by phitakphong on 17/05/2559.
 */
public class ProductRecyclerViewAdapter extends RecyclerView.Adapter<ProductRecyclerViewAdapter.ViewHolder> {

    private List<Product> items;
    private LayoutInflater inflater;
    private ListClickListener<Product> clickListener;

    public ProductRecyclerViewAdapter(List<Product> items) {
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_product_item, parent, false);
        return new ViewHolder(itemView, parent.getContext());
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Product product = items.get(position);
        if (this.clickListener != null) {
            holder.setClickListener(new ListClickListener<Product>() {
                @Override
                public void onClick(Product item) {
                    clickListener.onClick(item);
                }
            });
        }
        holder.init(product);

    }

    public void setClickListener(ListClickListener<Product> longClickListener) {
        this.clickListener = longClickListener;
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_food_icon)
        ImageView ivFoodIcon;
        @BindView(R.id.tv_food_name)
        TextView tvFoodName;
        @BindView(R.id.tv_qty)
        TextView tvQty;
        @BindView(R.id.list_item)
        LinearLayout listItem;
        @BindView(R.id.progress_bar)
        ProgressBar progressBar;
        @BindView(R.id.layout_qty)
        SquareLinerLayout layoutQty;

        private ListClickListener<Product> clickListener;

        private Context context;

        public ViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            ButterKnife.bind(this, view);
        }

        @SuppressLint("SetTextI18n")
        private void init(final Product product) {

            Util.loadImageFromUrl(context, PreferenceManager.getInstance().getBaseUrl() + "/files/products/" + product.getPhotothumb(), ivFoodIcon, progressBar);

            String prodName = product.getName();

            if (product.getType().equals("0")) {
                prodName += " (" + product.getQty() + ")";
            }
            tvFoodName.setText(prodName);

            if (product.getType().equals("0") && product.getQty() <= 0) {
                tvFoodName.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            }

            layoutQty.setVisibility(product.getOrderQty() > 0 ? View.VISIBLE : View.GONE);
            tvQty.setText(String.valueOf(product.getOrderQty()));

            listItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (product.getType().equals("1") || (product.getType().equals("0") && product.getQty() > product.getOrderQty())) {
                        int orderQty = product.getOrderQty() + 1;
                        product.setOrderQty(orderQty);
                        layoutQty.setVisibility(View.VISIBLE);
                        tvQty.setText(String.valueOf(product.getOrderQty()));
                        clickListener.onClick(product);
                    }
                }
            });

            listItem.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    product.setOrderQty(0);
                    layoutQty.setVisibility(View.GONE);
                    tvQty.setText("0");
                    clickListener.onClick(product);
                    return true;
                }
            });

        }

        public void setClickListener(ListClickListener<Product> clickListener) {
            this.clickListener = clickListener;
        }
    }
}