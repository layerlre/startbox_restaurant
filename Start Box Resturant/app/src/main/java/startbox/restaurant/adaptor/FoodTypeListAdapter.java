package startbox.restaurant.adaptor;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import startbox.restaurant.model.Category;
import startbox.restaurant.view.list_item.FoodTypeListItem;


public class FoodTypeListAdapter extends BaseAdapter {

    // Keep original data (un-modified by filtering)
    private List<Category> items;
    private ListClickListener<Category> clickListener;

    public FoodTypeListAdapter(List<Category> items) {
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }


    @Override
    public Object getItem(int position) {

        Category data = items.get(position);
        return data;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        FoodTypeListItem item;
        if (convertView != null)
            item = (FoodTypeListItem) convertView;
        else
            item = new FoodTypeListItem(parent.getContext());

        final Category category = items.get(position);
        item.setItem(category);
        item.setItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onClick(category);
            }
        });


        return item;
    }

    public void setItemClickListener(ListClickListener<Category> itemClickListener) {
        clickListener = itemClickListener;
    }


}
