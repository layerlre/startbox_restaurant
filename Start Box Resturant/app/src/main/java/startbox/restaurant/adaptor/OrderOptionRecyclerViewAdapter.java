package startbox.restaurant.adaptor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import startbox.restaurant.R;
import startbox.restaurant.model.Order;
import startbox.restaurant.model.OrderOption;
import startbox.restaurant.view.customview.SquareLinerLayout;


/**
 * Created by phitakphong on 17/05/2559.
 */
public class OrderOptionRecyclerViewAdapter extends RecyclerView.Adapter<OrderOptionRecyclerViewAdapter.ViewHolder> {

    private List<OrderOption> items;
    private LayoutInflater inflater;

    private ListClickListener<List<OrderOption>> selectedChangeListener;

    public OrderOptionRecyclerViewAdapter(List<OrderOption> items) {
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_order_option_item, parent, false);
        return new ViewHolder(itemView, parent.getContext());
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final OrderOption item = items.get(position);

        if (selectedChangeListener != null) {
            holder.setOnClickListener(new ListClickListener<OrderOption>() {
                @Override
                public void onClick(OrderOption item) {
                    notifyItemChanged(position);
                    selectedChangeListener.onClick(getSelectedItems());
                }
            });

        }

        holder.init(item);

    }

    public List<OrderOption> getSelectedItems() {
        List<OrderOption> selected = new ArrayList<>();
        for (OrderOption option : items) {
            if (option.getQty() > 0) {
                selected.add(option);
            }
        }
        return selected;
    }

    public void setSelectedChange(ListClickListener<List<OrderOption>> selectedChangeListener) {
        this.selectedChangeListener = selectedChangeListener;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.list_item)
        RelativeLayout listItem;
        @BindView(R.id.tv_option_name)
        TextView tvOptionName;
        @BindView(R.id.tv_qty)
        TextView tvQty;

        private Context context;
        private ListClickListener<OrderOption> clickListener;

        public ViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            ButterKnife.bind(this, view);
        }

        private void init(final OrderOption item) {

            if (item.getOptionprice() > 0) {
                tvOptionName.setText(item.getOptionname() + " (" + item.getOptionprice() + ")");
            } else {
                tvOptionName.setText(item.getOptionname());
            }

            tvQty.setVisibility(item.getQty() > 0 ? View.VISIBLE : View.GONE);
            tvQty.setText(String.valueOf(item.getQty()));

            if(this.clickListener != null){
                listItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        item.setQty(item.getQty() + 1);
                        clickListener.onClick(item);
                    }
                });

                listItem.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        item.setQty(0);
                        clickListener.onClick(item);
                        return true;
                    }
                });
            }
        }

        public void setOnClickListener(ListClickListener<OrderOption> clickListener) {
            this.clickListener = clickListener;
        }


    }
}