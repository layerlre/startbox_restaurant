package startbox.restaurant.adaptor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import startbox.restaurant.R;
import startbox.restaurant.model.Table;


/**
 * Created by phitakphong on 17/05/2559.
 */
public class TableRecyclerViewAdapter extends RecyclerView.Adapter<TableRecyclerViewAdapter.ViewHolder> {

    private List<Table> items;
    private LayoutInflater inflater;
    private ListClickListener<Table> clickListener;

    public TableRecyclerViewAdapter(List<Table> items) {
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_table_list_item, parent, false);
        return new ViewHolder(itemView, parent.getContext());
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Table table = items.get(position);
        holder.init(table);

        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onClick(table);
            }
        });

    }

    public void setOnClickListener(ListClickListener<Table> clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_table_icon)
        ImageView ivTableIcon;
        @BindView(R.id.tv_table_name)
        TextView tvTableName;
        @BindView(R.id.list_item)
        LinearLayout listItem;

        private Context context;

        public ViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            ButterKnife.bind(this, view);
        }

        private void init(Table table) {
            if (table.getStatus() == 0) {
                ivTableIcon.setImageResource(R.drawable.table);
            } else {
                ivTableIcon.setImageResource(R.drawable.tableb3);
            }
            tvTableName.setText(table.getName());
        }

        public void setOnClickListener(final View.OnClickListener onClickListener){
            if(onClickListener != null){
                listItem.setOnClickListener(onClickListener);
            }
        }
    }
}