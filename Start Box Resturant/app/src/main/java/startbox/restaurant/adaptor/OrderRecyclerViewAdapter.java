package startbox.restaurant.adaptor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import startbox.restaurant.R;
import startbox.restaurant.model.Order;
import startbox.restaurant.util.PreferenceManager;
import startbox.restaurant.util.Util;


/**
 * Created by phitakphong on 17/05/2559.
 */
public class OrderRecyclerViewAdapter extends RecyclerView.Adapter<OrderRecyclerViewAdapter.ViewHolder> {

    private List<Order> items;
    private LayoutInflater inflater;
    private ListClickListener<Order> clickListener;
    private ListClickListener<Order> removeListener;
    private ListClickListener<Order> printItemListener;

    public OrderRecyclerViewAdapter(List<Order> items) {
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_order_list_item, parent, false);
        return new ViewHolder(itemView, parent.getContext());
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Order order = items.get(position);
        holder.init(order);

        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickListener.onClick(order);
            }
        });

        holder.setOnRemoveClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeListener.onClick(order);
            }
        });
        holder.setOnPrintClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                printItemListener.onClick(order);
            }
        });

    }

    public void setOnClickListener(ListClickListener<Order> clickListener) {
        this.clickListener = clickListener;
    }

    public void setOnRemoveListener(ListClickListener<Order> clickListener) {
        this.removeListener = clickListener;
    }


    public void setPrintItemListener(ListClickListener<Order> clickListener) {
        this.printItemListener = clickListener;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_food_icon)
        ImageView ivFoodIcon;
        @BindView(R.id.iv_remove)
        ImageView ivRemove;
        @BindView(R.id.iv_print)
        ImageView ivPrint;
        @BindView(R.id.tv_food_name)
        TextView tvFoodName;
        @BindView(R.id.tv_food_desc)
        TextView tvFoodDesc;
        @BindView(R.id.list_item)
        LinearLayout listItem;

        private Context context;

        public ViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            ButterKnife.bind(this, view);
        }

        private void init(Order order) {
            if(order.getCookstatus() == 1){
                ivPrint.setVisibility(View.VISIBLE);
            }else {
                ivPrint.setVisibility(View.GONE);
            }
            Util.loadImageFromUrl(context, PreferenceManager.getInstance().getBaseUrl() + "/files/products/" + order.getPhotothumb(), ivFoodIcon);
            tvFoodName.setText(order.getName() + " (" + order.getQt() + ")");
            tvFoodDesc.setText(order.getOptions());
        }

        public void setOnClickListener(final View.OnClickListener onClickListener) {
            if (onClickListener != null) {
                listItem.setOnClickListener(onClickListener);
            }
        }

        public void setOnRemoveClickListener(final View.OnClickListener onClickListener) {
            if (onClickListener != null) {
                ivRemove.setOnClickListener(onClickListener);
            }
        }

        public void setOnPrintClickListener(final View.OnClickListener onClickListener) {
            if (onClickListener != null) {
                ivPrint.setOnClickListener(onClickListener);
            }
        }
    }
}