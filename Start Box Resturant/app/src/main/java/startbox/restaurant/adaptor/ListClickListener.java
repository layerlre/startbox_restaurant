package startbox.restaurant.adaptor;

/**
 * Created by Phitakphong on 13/11/2560.
 */

public interface ListClickListener<T> {
    void onClick(T item);
}
