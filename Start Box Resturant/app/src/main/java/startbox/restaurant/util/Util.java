package startbox.restaurant.util;

import android.Manifest;
import android.content.Context;
import android.support.annotation.DrawableRes;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import startbox.restaurant.R;

public class Util {

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private static Gson gson;

    public static Gson createGson() {
        if (gson == null) {
            gson = new GsonBuilder()
                    .setDateFormat(Constant.JSON_DATE_TIME_FORMAT)
                    .create();
        }
        return gson;
    }

    public static void showToast(String text) {
        Toast.makeText(Contextor.getInstance().getContext()
                , text
                , Toast.LENGTH_SHORT)
                .show();
    }

    public static String getErrorMessage(ResponseBody errorBody) {
        try {
            return errorBody.string();
        } catch (IOException e) {
            e.printStackTrace();
            return  e.getMessage();
        }
    }

    public static void loadImageFromUrl(Context context, String image, final ImageView imageView, final ProgressBar progressBar) {

        if (context == null) {
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        Glide.with(context)
                .load(image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.ic_not_found)
                .dontAnimate()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(imageView);
    }

    public static void loadImageFromUrl(Context context, String image, final ImageView imageView) {

        if (context == null) {
            return;
        }

        Glide.with(context)
                .load(image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.ic_not_found)
                .dontAnimate()
                .into(imageView);
    }


}
