package startbox.restaurant.util;

public class RequestCode {
    public static final int FOOD_TYPE = 1000;
    public static final int FOOD_SELECTOR = 1001;
    public static final int SWAP_TABLE = 1002;
    public static final int ORDER_OPTION = 1003;
}
