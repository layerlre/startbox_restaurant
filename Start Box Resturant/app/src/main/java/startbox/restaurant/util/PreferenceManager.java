package startbox.restaurant.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import startbox.restaurant.model.User;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class PreferenceManager {

    private static PreferenceManager instance;

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    public static PreferenceManager getInstance() {
        if (instance == null)
            instance = new PreferenceManager();
        return instance;
    }

    private Context mContext;

    private PreferenceManager() {
        mContext = Contextor.getInstance().getContext();

        prefs = mContext.getSharedPreferences("preference", Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    /****************************
     * Preferences
     ****************************/

    public String getBaseUrl() {
        return prefs.getString("BaseUrl", null);
    }

    public void setBaseUrl(String baseUrl) {
        if (baseUrl == null) {
            editor.remove("BaseUrl");
        } else {
            editor.putString("BaseUrl", baseUrl);
        }
        editor.apply();
    }


    public User getUser() {
        String json = prefs.getString("User", null);
        if (json == null) {
            return null;
        }
        Gson gson = new Gson();
        return gson.fromJson(json, User.class);
    }

    public void setUser(User user) {
        if (user == null) {
            editor.remove("User");
        } else {
            Gson gson = new Gson();
            editor.putString("User", gson.toJson(user));
        }
        editor.apply();
    }
}
